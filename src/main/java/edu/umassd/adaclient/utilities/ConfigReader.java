package edu.umassd.adaclient.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import edu.umassd.adaclient.MainApp;
import edu.umassd.adaclient.database.DBConnection;
import edu.umassd.adaclient.gui.DataSorter;
import edu.umassd.adaclient.gui.DataSorter2;
import edu.umassd.adaclient.gui.EditNode;
import edu.umassd.adaclient.gui.ReportNode;
import edu.umassd.adaclient.gui.ViewNode;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ConfigReader {

	private FileReader fr = null;
	private BufferedReader br = null;
	private final String FILENAME = "res/dbaccess.cfg";
	private ObservableList<DataSorter> data;
	private ObservableList<DataSorter2> editData;

	private ViewNode vn;
	private EditNode en;
	private ReportNode rn;
	
	private ReportData rnData1;
	private ReportData rnData2;

	public ConfigReader() {
		reset();
		readConfig();
	}

	public int loadLastPage()
	{
		reset();
		int temp = Integer.parseInt(readStartingWith("lastPage"));
		close();
		return temp;
	}
	
	//only loads View Node state right now
	public void readConfig() {
		
		DBConnection.IP_ADDRESS = readStartingWith("IP_Address");
		DBConnection.PORT = readStartingWith("Port");
		DBConnection.USERNAME = readStartingWith("Username");
		DBConnection.PASSWORD = readStartingWith("Password");
		close();
	
	}

	// takes in the prefix of the string returns the rest of the line
	private String readStartingWith(String prefix) {
		String line = "";
		String temp = "";
		try {
			while ((line = br.readLine()) != null) {
				if (line.startsWith(prefix)) {
					boolean copy = false;
					for (int i = 0; i < line.length(); i++) {
						if (copy)
							temp += line.charAt(i);

						if (line.charAt(i) == ':')
							copy = true;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		reset();
		return temp;
	}

	private void reset() {
		try {
			File temp = new File(FILENAME);

			if (!temp.exists())
				temp.createNewFile();

			fr = new FileReader(temp);
			br = new BufferedReader(fr);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// takes in the prefix of the string returns all of the lines containing it
	private ArrayList<String> readAllStartingWith(String prefix) {
		String line = "";
		String temp = "";
		reset();
		ArrayList<String> tableList = new ArrayList<String>();
		try {
			while ((line = br.readLine()) != null) {
				if (line.startsWith(prefix)) {
					boolean copy = false;
					for (int i = 0; i < line.length(); i++) {
						if (copy)
							temp += line.charAt(i);

						if (line.charAt(i) == ':')
							copy = true;
					}
					tableList.add(temp);
					temp = "";
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		reset();
		return tableList;
	}

	public void close() {
		try {
			br.close();
			fr.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
