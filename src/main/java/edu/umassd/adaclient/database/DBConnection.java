
/*Contains the methods for creating a connection to, and disconnecting from the database*/

package edu.umassd.adaclient.database;

import java.sql.*;
import java.util.concurrent.Executors;

import edu.umassd.adaclient.utilities.ConfigReader;

public class DBConnection {
	static Connection c;
	static boolean flag = true;
	public static String IP_ADDRESS = "";
	public static String PORT = "";
	public static String USERNAME = "";
	public static String PASSWORD = "";

	public static Connection dbConnect() {
		
		ConfigReader r = new ConfigReader();
		r = null;

		try {
			String databaseURL = "jdbc:mysql://"+IP_ADDRESS+":"+PORT+"/aisdata?user="+USERNAME+"&password="+PASSWORD+"&autoreconnect=true&useSSL=false";
			c = DriverManager.getConnection(databaseURL);

			if (c != null) {
				flag = false;
				System.out.println("Connected to the database");
			}
			return c;
		} catch (SQLException se) {
			System.out.println("An error occurred");
			se.printStackTrace();
			if (c == null) {
				while (flag) {
					System.out.println("Attempting to reconnect to the database");
					dbConnect();
				}
			}
			se.printStackTrace();
			return null;
		}
	}

	public static Connection reconnect() {

		try {
			String databaseURL = "jdbc:mysql://"+IP_ADDRESS+":"+PORT+"/aisdata?user="+USERNAME+"&password="+PASSWORD+"&autoreconnect=true&useSSL=false";
			// Open a connection to database
			c = DriverManager.getConnection(databaseURL);
			c.setNetworkTimeout(Executors.newFixedThreadPool(1), 1000000000);
			if (c != null) {
				flag = false;
				System.out.println("Connected to the database");
			}
			return c;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static void dbDisconnect(Connection c) {
		try {
			if (c != null) {
				c.close();
				System.out.println("Successfully disconnected from database!");
			}
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}
}